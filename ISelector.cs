using Microsoft.Extensions.DependencyInjection;

namespace Chims.Extensions.DI
{
    /// <summary>
    ///     选择器
    /// </summary>
    internal interface ISelector
    {
        /// <summary>
        /// </summary>
        /// <param name="services"></param>
        void Populate(IServiceCollection services);
    }
}